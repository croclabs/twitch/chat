## Chat customizer for Twitch!

Base for customizing Twitch chats! Download the project as a .zip, make your CSS changes and enter your channel name and you are good to go!

In OBS, set the index.html as an browser source and setup the correct width and height.
