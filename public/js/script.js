let chat = document.querySelector("#chat>ul");
let showBadges = true;
let chatWrapper = document.querySelector("#chat");

let style = document.createElement('style');
style.innerHTML = `
  #chat span.name {
    font-family: ${config.names.font};
    font-size: ${config.names.fontSize};
    position: relative;
    z-index: 1;
    padding: 5px 10px;
    border-radius: ${config.names.borderRadius};
    color: ${config.names.color};
    width: fit-content;
    background-image: url('${config.names.backgroundImage.url}');
    background-repeat: ${config.names.backgroundImage.backgroundRepeat};
    background: ${config.names.backgroundColor};
  }

  #chat span.name.bot {
    background: ${config.names.botBackgroundColor};
  }

  #chat span.name.broadcaster {
    background: ${config.names.broadcasterBackgroundColor};
  }

  #chat span.message {
    font-family: ${config.messages.font};
    font-size: ${config.messages.fontSize};
    position: relative;
    top: -0.5rem;
    left: 0.5rem;
    padding: 7px 10px;
    border-radius: ${config.messages.borderRadius};
    color: ${config.messages.color};
    width: fit-content;
    word-wrap: anywhere;
    background-image: url('${config.messages.backgroundImage.url}');
    background-repeat: ${config.messages.backgroundImage.backgroundRepeat};
    background: ${config.messages.backgroundColor};
  }
`;

chatWrapper.append(style);

ComfyJS.onChat = (user, message, flags, self, extra) => {
    // console.log(user, message, flags, self, extra);
    // console.warn(user, message);

    let newMessage = document.createElement("li");
    let name = document.createElement("span");
    name.classList.add("name");

    if (config.names.botNames.includes(user)) {
        name.classList.add('bot');
    }

    if (flags.broadcaster) {
        name.classList.add('broadcaster');
    }

    name.textContent = user;
    let msg = document.createElement("span");
    msg.className = "message";

    // console.warn(getMessageHTML(message, extra.messageEmotes));

    msg.innerHTML = getMessageHTML(message, extra.messageEmotes);
    newMessage.append(name, msg);
    chat.append(newMessage);

    if (document.querySelector('#chat ul').children.length > 10) {
      document.querySelector('#chat ul li').remove();
    }
}

function getMessageHTML(message, emotes) {
    if (!emotes) return message;
  
    // store all emote keywords
    // ! you have to first scan through 
    // the message string and replace later
    const stringReplacements = [];
  
    // iterate of emotes to access ids and positions
    Object.entries(emotes).forEach(([id, positions]) => {
      // use only the first position to find out the emote key word
      const position = positions[0];
      const [start, end] = position.split("-");
      const stringToReplace = message.substring(
        parseInt(start, 10),
        parseInt(end, 10) + 1
      );
  
      stringReplacements.push({
        stringToReplace: stringToReplace,
        replacement: `<img src="https://static-cdn.jtvnw.net/emoticons/v2/${id}/default/light/1.0">`,
      });
    });
  
    // generate HTML and replace all emote keywords with image elements
    const messageHTML = stringReplacements.reduce(
      (acc, { stringToReplace, replacement }) => {
        // obs browser doesn't seam to know about replaceAll
        return acc.split(stringToReplace).join(replacement);
      },
      message
    );
  
    return messageHTML;
  }

// global: https://badges.twitch.tv/v1/badges/global/display
// channel: https://badges.twitch.tv/v1/badges/channels/{id}}/display (id kommt aus dem extra object, "roomId")
// die badges kann man via property namen und dann mit "image_url_[value]x" finden
function badges() {
    // TODO
}

if (!config.channelName) {
    alert('Please provide a channel name in config.js!');
} else {
    ComfyJS.Init(config.channelName);
}