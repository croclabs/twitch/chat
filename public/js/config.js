let config = {
    channelName: "",
    names: {
        backgroundColor: 'blueviolet',
        color: 'white',
        botBackgroundColor: 'darkgray',
        broadcasterBackgroundColor: 'red',
        font: 'monospace',
        fontSize: '12px',
        botNames: ['Moobot', 'Nightbot'],
        backgroundImage: {
            url: '',
            backgroundRepeat: 'repeat'
        },
        borderRadius: '10px'
    },
    messages: {
        backgroundColor: 'wheat',
        color: 'black',
        font: 'monospace',
        fontSize: '12px',
        backgroundImage: {
            url: '',
            backgroundRepeat: 'repeat'
        },
        borderRadius: '10px'
    }
}